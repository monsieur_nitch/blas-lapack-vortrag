PROGRAM EIGENPROBLEMS
  
  IMPLICIT NONE

  INTEGER, PARAMETER :: N = 3 
  COMPLEX(KIND=8), DIMENSION(N, N) :: A

  COMPLEX(KIND=8), DIMENSION(N) :: W
  COMPLEX(KIND=8) :: VL = (0.0_8, 0.0_8)
  COMPLEX(KIND=8), DIMENSION(N, N) :: VR

  COMPLEX(KIND=8), DIMENSION(390) :: WORK = 0.0_8
  REAL(KIND=8), DIMENSION(2*N) :: RWORK
  INTEGER :: INFO

  A = reshape((/( 0.0_8, 0.0_8), ( 2.0_8, 0.0_8), ( 2.0_8, 0.0_8), &
                ( 2.0_8, 0.0_8), (-1.0_8, 0.0_8), (-1.0_8, 0.0_8), &
                (-1.0_8, 0.0_8), ( 1.0_8, 0.0_8), ( 3.0_8, 0.0_8) /), &
              (/3, 3/))

  CALL ZGEEV('N', 'V', 3, A, 3, W, VL, 1, VR, 3, WORK, 390, RWORK, INFO)

  WRITE(*, '("Lambda(1):",F12.7,SP,F12.7,"i")') W(1)
  WRITE(*, '("Lambda(2):",F12.7,SP,F12.7,"i")') W(2)
  WRITE(*, '("Lambda(3):",F12.7,SP,F12.7,"i")') W(3)
  WRITE(*, '(F12.7,SP,F12.7,"i,",F12.7,SP,F12.7,"i,",F12.7,SP,F12.7,"i")') VR(1, 1), VR(2, 1), VR(3, 1)
  WRITE(*, '(F12.7,SP,F12.7,"i,",F12.7,SP,F12.7,"i,",F12.7,SP,F12.7,"i")') VR(1, 2), VR(2, 2), VR(3, 2)
  WRITE(*, '(F12.7,SP,F12.7,"i,",F12.7,SP,F12.7,"i,",F12.7,SP,F12.7,"i")') VR(1, 3), VR(2, 3), VR(3, 3)

END PROGRAM EIGENPROBLEMS
