#include <stdio.h>

int main() {
  int m = 3;
  int n = 2;
  int k = 2;
  
  double A[6] = {1.0, 3.0, -2.0, 1.0, 2.0, 1.0};
  double B[4] = {1.0, 2.0, -1.0, 3.0};
  double C[6];
  
  double dgemm_(char *transa, char *transb, int *m, int *n, int *k, double *alpha, double *A, int *lda, double *B, int *ldb, double *beta, double *C, int *ldc);

  char transa = 'n';
  char transb = 'n';

  double alpha = 1.0;
  int lda = 3;
  int ldb = 2;
  double beta = 0.0;
  int ldc = 3;

  dgemm_(&transa, &transb, &m, &n, &k, &alpha, A, &lda, B, &ldb, &beta, C, &ldc);

  for (int i=0; i<3; i++) {
    for (int j=0; j<2; j++) {
      printf("%12.7f", C[j * 3 + i]);
    }
    printf("\n");
  }

  printf("\n");

  return 0;
}
  
