#include <stdio.h>

int main() {
  int n = 3;
  double u[3] = {1.0, -1.0, 0.0};
  double v[3] = {3.0, 1.0, 1.0};
  double ddot_(int *n, double *dx, int *incx, double *dy, int *incy);

  int incu = 1;
  int incv = 1;
  double result = ddot_(&n, u, &incu, v, &incv);
  printf("%12.7f\n", result);
  return 0;
}

