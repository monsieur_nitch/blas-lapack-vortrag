PROGRAM DOTPRODUCT

  IMPLICIT NONE

  INTEGER, PARAMETER :: N = 3
  DOUBLE PRECISION, DIMENSION(N) :: U
  DOUBLE PRECISION, DIMENSION(N) :: V
  DOUBLE PRECISION :: DDOT
  DOUBLE PRECISION :: RESULT
  
  U(1) = 1.0
  U(2) = -1.0
  U(3) = 0.0
  
  V(1) = 3.0
  V(2) = 1.0
  V(3) = 1.0

  RESULT=DDOT(N, U, 1, V, 1)
  WRITE(*,"(F12.7)") RESULT

 END PROGRAM DOTPRODUCT
  
