PROGRAM LGS

  IMPLICIT NONE

  DOUBLE PRECISION, DIMENSION(3, 3) :: A
  DOUBLE PRECISION, DIMENSION(3) :: b

  INTEGER :: I
  INTEGER :: INFO
  INTEGER, DIMENSION(3) :: IPIV
  
  A = reshape((/1.0_8, 2.0_8, 0.0_8, 2.0_8, 3.0_8, 1.0_8, 4.0_8, -1.0_8, 2.0_8/), (/3, 3/))
  b = (/1.0_8, -5.0_8, 0.0_8/)
  
  CALL DGESV(3, 1, A, 3, IPIV, b, 3, INFO)

  WRITE(*,'(A)') "Result"
  DO I=1,3
     WRITE(*,"(F12.7)") b(I)
  END DO

END PROGRAM LGS
