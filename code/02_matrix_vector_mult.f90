PROGRAM MATRIX_VECTOR_MULT

  IMPLICIT NONE

  DOUBLE PRECISION, DIMENSION(2, 2) :: B = reshape((/1.0_8, 2.0_8, 0.0_8, 3.0_8/), (/2, 2/))
  DOUBLE PRECISION, DIMENSION(2) :: v = (/-1.0_8, 2.0_8/)
  DOUBLE PRECISION, DIMENSION(2) :: Bv

  INTEGER :: I
  INTEGER :: J

  DO I=1,2
     DO J=1,2
        WRITE(*,"(F12.7)", ADVANCE="NO") B(I, J)
     END DO
     WRITE(*,*)
  END DO
  WRITE(*,*)

  DO I=1,2
     WRITE(*,"(F12.7)") v(I)
  END DO
  WRITE(*,*)
  
  CALL DGEMV('N', 2, 2, 1.0_8, B, 2, v, 1, 0.0_8, Bv, 1)

  WRITE(*,'(A)') "Result"
  DO I=1,2
     WRITE(*,"(F12.7)") Bv(I)
  END DO

END PROGRAM MATRIX_VECTOR_MULT
